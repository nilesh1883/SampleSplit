# SampleSplit

Steps: 

- Drag and drop "SlideViewController.h" and "SlideViewController.m" in your project hierarchy.
- Include "SlideViewController.h" in the file where you want to initialise SlideViewController.
- SlideViewController is singleton class your "sharedSlideViewController" API to create object.
- Create the object of your list view controller and add it as a masterViewController in "SlideViewController", as shown in sample app. 
- Create the object of your detail view controller and add it as a detailViewController in "SlideViewController", as shown in sample app. 
- Enjoy.
