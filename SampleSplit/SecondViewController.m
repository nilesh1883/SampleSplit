//
//  SecondViewController.m
//  SampleSplit
//
//  Created by Nilesh Jaiswal on 2/23/16.
//  Copyright © 2016 Nilesh Jaiswal. All rights reserved.
//

#import "SecondViewController.h"
#import "SlideViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor yellowColor]];
    
    self.title = @"Second ViewController";

    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(rightBarButtonItemSelected:)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
}

- (void)rightBarButtonItemSelected:(id) sender {
        [[SlideViewController sharedSlideViewController] toggleHideLeftMenuButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
