//
//  SlideViewController.h
//  SampleSplit
//
//  Created by Nilesh Jaiswal on 2/19/16.
//  Copyright © 2016 Nilesh Jaiswal. All rights reserved.
//

#import <UIKit/UIKit.h>

// These are two notifications can be used to notify open/close of split view
#define SpliteViewControllerDidOpen  @"SpliteViewControllerDidOpen"
#define SpliteViewControllerDidClose @"SpliteViewControllerDidClose"

@interface SlideViewController : UIViewController {

}

// This property is used to set the master view controller, which goes in left hand side.
@property(nonatomic, strong) UIViewController *masterVewController;
// This property is used to set the detail view controller, which goes in right hand side.
@property(nonatomic, strong) UIViewController *detailVewController;


// It is a singleton class user should use this API to get the active object.
+ (SlideViewController *)sharedSlideViewController;

// This API is used to toggle sandwich menu bar button item.
- (void)toggleHideLeftMenuButton;

// To get the current visible viewController.
- (UIViewController *)getVisibleViewController;

@end

