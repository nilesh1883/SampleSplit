//
//  AppDelegate.h
//  SampleSplit
//
//  Created by Nilesh Jaiswal on 2/19/16.
//  Copyright © 2016 Nilesh Jaiswal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

