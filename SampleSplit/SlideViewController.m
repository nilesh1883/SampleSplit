//
//  SlideViewController.m
//  SampleSplit
//
//  Created by Nilesh Jaiswal on 2/19/16.
//  Copyright © 2016 Nilesh Jaiswal. All rights reserved.
//

#pragma Device & OS specific

#define IS_IOS8 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_7_1)
#define IS_IOS7 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
#define IS_IPHONE_5 ((!IS_IPAD) && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPAD_MINI (IS_IPAD && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0f)

#define MasterVCTag    999
#define DetailVCTag    888

#define OPTIONS_TABLE_WIDTH     250
#define SLIDE_MOVEMENT  100
#define MENU_SLIDE_ANIMATION_DURATION 0.3
#define MENU_QUICK_SLIDE_ANIMATION_DURATION 0.18
#define MENU_SHADOW_RADIUS 10
#define MENU_SHADOW_OPACITY 1
#define MENU_DEFAULT_SLIDE_OFFSET (self.detailVewController.view.frame.size.width - OPTIONS_TABLE_WIDTH)
#define MENU_FAST_VELOCITY_FOR_SWIPE_FOLLOW_DIRECTION 1000
#define STATUS_BAR_HEIGHT 20
#define PAN_GESTURE_SLIDE_OFFSET 60
#define STATUSBAR_FRAME [[UIApplication sharedApplication] statusBarFrame]

#import "SlideViewController.h"

@interface SlideViewController() <UIGestureRecognizerDelegate> {
    
}

@property (nonatomic, assign) BOOL menuNeedsLayout;
@property (nonatomic, assign) BOOL enableSwipeGesture;
@property (nonatomic, assign) BOOL enableShadow;

@property (nonatomic, assign) CGPoint draggingPoint;
@property (nonatomic, assign) CGFloat statusBarHeight;
@property (nonatomic, assign) CGFloat portraitSlideOffset;
@property (nonatomic, assign) CGFloat landscapeSlideOffset;
@property (nonatomic, assign) CGFloat panGestureSideOffset;
@property (nonatomic, assign) CGFloat menuRevealAnimationDuration;

@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *panRecognizer;
@property (nonatomic, assign) UIViewAnimationOptions menuRevealAnimationOption;

- (void)setup;
- (UIBarButtonItem *)getLeftBarButtonItem;
- (CGFloat)horizontalLocation;
- (CGFloat)horizontalSize;
- (NSInteger)minXForDragging;
- (NSInteger)maxXForDragging;
- (UINavigationController *)getCurrentNavigationController;

@end

@implementation SlideViewController

@synthesize masterVewController = _masterVewController;
@synthesize detailVewController = _detailVewController;
@synthesize tapRecognizer = _tapRecognizer;
@synthesize panRecognizer = _panRecognizer;
@synthesize draggingPoint = _draggingPoint;
@synthesize menuNeedsLayout = _menuNeedsLayout;
@synthesize statusBarHeight = _statusBarHeight;
@synthesize enableSwipeGesture = _enableSwipeGesture;
@synthesize enableShadow = _enableShadow;
@synthesize portraitSlideOffset = _portraitSlideOffset;
@synthesize landscapeSlideOffset = _landscapeSlideOffset;
@synthesize panGestureSideOffset = _panGestureSideOffset;
@synthesize menuRevealAnimationDuration = _menuRevealAnimationDuration;
@synthesize menuRevealAnimationOption = _menuRevealAnimationOption;

#pragma mark - Singleton Methods -

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

- (id)init {
    if (self = [super init]) {
        [self setup];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder])
    {
        [self setup];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor purpleColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    if (self.enableShadow) {
        self.detailVewController.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.detailVewController.view.bounds].CGPath;
    }
    
    [self enableTapGestureToCloseMenu:YES];
    
    self.menuNeedsLayout = !self.menuNeedsLayout;
}

#pragma mark - Shared APIs -

+ (id)sharedSlideViewController {
    static SlideViewController *singletonInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singletonInstance = [[self alloc] init];
    });
    return singletonInstance;
}

- (void)setMasterVewController:(UIViewController *) masterVewController {
    if (nil != masterVewController)
    {
        UIView *topView = [self.view viewWithTag:MasterVCTag];
        [topView removeFromSuperview];

        if (nil != _masterVewController) {
            [_masterVewController removeFromParentViewController];
            _masterVewController = nil;
        }

        _masterVewController = masterVewController;
        topView = self.masterVewController.view;
        topView.tag = MasterVCTag;
        [self addChildViewController:self.masterVewController];
        CGRect frame = self.view.bounds;
        frame.size.width = OPTIONS_TABLE_WIDTH;
        topView.frame = frame;
        topView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        
        [self.view insertSubview:topView atIndex:0];
        [self.view sendSubviewToBack:topView];
        [self.masterVewController didMoveToParentViewController:self];
    }
}

- (void)setDetailVewController:(UIViewController *) detailVewController {
    if (nil != detailVewController)
    {
        CGRect preFrame = self.view.bounds;
        UIView *topView = [self.view viewWithTag:DetailVCTag];

        if (nil != _detailVewController) {
            preFrame = _detailVewController.view.frame;
            [topView removeFromSuperview];
            [_detailVewController removeFromParentViewController];
            _detailVewController = nil;
        }

        _detailVewController = detailVewController;
        topView = self.detailVewController.view;
        topView.tag = DetailVCTag;
        
        [self addChildViewController:self.detailVewController];
        
        topView.frame = preFrame;
        
        [self.view addSubview:topView];
        [self.detailVewController didMoveToParentViewController:self];

        [self enableTapGestureToCloseMenu:YES];
        [self setEnableSwipeGesture:YES];
        self.enableShadow = YES;
        
        
        if (nil != [self getCurrentNavigationController])
        {
            UIViewController *visibleVC = [self getVisibleViewController];
            [visibleVC.navigationItem setLeftBarButtonItem:[self getLeftBarButtonItem]];
        }
        
        if ([self isMenuOpen])
        {
            [self closeMenuWithCompletion:^{
            }];
        }
    }
}

- (UIViewController *)getVisibleViewController {
    UIViewController *retViewController = self.detailVewController;
    
    if (([self.detailVewController isKindOfClass:[UIViewController class]] || [self.detailVewController isKindOfClass:[UICollectionViewController class]] || [self.detailVewController isKindOfClass:[UITableViewController class]]) && (NO == [self.detailVewController respondsToSelector:@selector(initWithNavigationBarClass:toolbarClass:)])) {
        retViewController = self.detailVewController;
    }
    else if ([self.detailVewController isKindOfClass:[UINavigationController class]])
    {
        retViewController = [(UINavigationController *)self.detailVewController visibleViewController];
    }
    else if ([self.detailVewController isKindOfClass:[UITabBarController class]])
    {
        retViewController = [(UITabBarController *)self.detailVewController selectedViewController];
        
        if (([retViewController isKindOfClass:[UIViewController class]] || [retViewController isKindOfClass:[UICollectionViewController class]] || [retViewController isKindOfClass:[UITableViewController class]]) && (NO == [self.detailVewController respondsToSelector:@selector(initWithNavigationBarClass:toolbarClass:)]))
        {
            retViewController = self.detailVewController;
        }
        else if ([retViewController isKindOfClass:[UINavigationController class]])
        {
            retViewController = [(UINavigationController *)self.detailVewController visibleViewController];
        }
    }
    
    return retViewController;
}

- (UINavigationController *)getCurrentNavigationController {
    UINavigationController *retViewController = nil;
    
    if (([self.detailVewController isKindOfClass:[UIViewController class]] || [self.detailVewController isKindOfClass:[UICollectionViewController class]] || [self.detailVewController isKindOfClass:[UITableViewController class]]) && (NO == [self.detailVewController respondsToSelector:@selector(initWithNavigationBarClass:toolbarClass:)]))  {
        retViewController = nil;
    }
    else if ([self.detailVewController isKindOfClass:[UINavigationController class]])
    {
        retViewController = (UINavigationController *)self.detailVewController;
    }
    else if ([self.detailVewController isKindOfClass:[UITabBarController class]])
    {
        retViewController = [(UITabBarController *)self.detailVewController selectedViewController];
        
        if (([retViewController isKindOfClass:[UIViewController class]] || [retViewController isKindOfClass:[UICollectionViewController class]] || [retViewController isKindOfClass:[UITableViewController class]]) && (NO == [self.detailVewController respondsToSelector:@selector(initWithNavigationBarClass:toolbarClass:)]))
        {
            retViewController = nil;
        }
        else if ([retViewController isKindOfClass:[UINavigationController class]])
        {
            retViewController = (UINavigationController *)[(UINavigationController *)self.detailVewController visibleViewController];
        }
    }
    return retViewController;
}

#pragma mark - Orientation Methods -

- (BOOL)shouldAutorotate {
    return [[self getVisibleViewController] shouldAutorotate];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return [[self getVisibleViewController] supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return [[self getVisibleViewController] preferredInterfaceOrientationForPresentation];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    self.menuNeedsLayout = YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    self.menuNeedsLayout = YES;
}

#pragma mark - Private Methodes -

- (void)setup {
    self.masterVewController = nil;
    self.detailVewController = nil;
    
    self.statusBarHeight = STATUS_BAR_HEIGHT;
    self.menuRevealAnimationDuration = MENU_SLIDE_ANIMATION_DURATION;
    self.landscapeSlideOffset = MENU_DEFAULT_SLIDE_OFFSET;
    self.portraitSlideOffset = MENU_DEFAULT_SLIDE_OFFSET;
    self.panGestureSideOffset = PAN_GESTURE_SLIDE_OFFSET;
    self.menuRevealAnimationOption = UIViewAnimationOptionCurveEaseOut;
    self.enableShadow = YES;
    
    self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panDetected:)];
    self.panRecognizer.delegate = self;
    self.enableSwipeGesture = YES;
}

- (void)openMenuWithCompletion:(void (^)())completion {
    [self openMenuWithDuration:self.menuRevealAnimationDuration andCompletion:completion];
}

- (void)closeMenuWithCompletion:(void (^)())completion {
    [self closeMenuWithDuration:self.menuRevealAnimationDuration andCompletion:completion];
}

- (void)moveHorizontallyToLocation:(CGFloat)location {
    CGRect rect = self.detailVewController.view.frame;
    rect.origin.x = location;
    self.detailVewController.view.frame = rect;
    [self updateMenuAnimation];
}

- (void)openMenuWithDuration:(float)duration andCompletion:(void (^)())completion {
    [self enableTapGestureToCloseMenu:YES];
    [UIView animateWithDuration:duration
                          delay:0
                        options:self.menuRevealAnimationOption
                     animations:^{
                         CGRect rect = self.detailVewController.view.frame;
                         CGFloat width = self.horizontalSize;
                         rect.origin.x = (width - self.slideOffset);
                         [self moveHorizontallyToLocation:rect.origin.x];
                         
                         CGRect leftMenuFrame = self.detailVewController.view.frame;
                         leftMenuFrame.origin.y = rect.origin.y;
                         self.detailVewController.view.frame = leftMenuFrame;
                     }
                     completion:^(BOOL finished) {
                         if (completion) {
                             completion();
                             [[self getVisibleViewController].view setUserInteractionEnabled:NO];
                             [[NSNotificationCenter defaultCenter] postNotificationName:SpliteViewControllerDidOpen
                                                                                 object:nil
                                                                               userInfo:nil];
                         }
                     }];
}

- (void)closeMenuWithDuration:(float)duration andCompletion:(void (^)())completion {
    [self enableTapGestureToCloseMenu:NO];
    
    [UIView animateWithDuration:duration
                          delay:0
                        options:self.menuRevealAnimationOption
                     animations:^{
                         CGRect rect = self.detailVewController.view.frame;
                         rect.origin.x = 0;
                         [self moveHorizontallyToLocation:rect.origin.x];
                     }
                     completion:^(BOOL finished) {
                         if (completion) {
                             completion();
                             [[self getVisibleViewController].view setUserInteractionEnabled:YES];
                             [[NSNotificationCenter defaultCenter] postNotificationName:SpliteViewControllerDidClose
                                                                                 object:nil
                                                                               userInfo:nil];
                         }
                     }];
}

- (void)updateMenuAnimation {
    CGFloat progress = (self.horizontalLocation / (self.horizontalSize - self.slideOffset));
    [self animateMenuWithProgress:progress];
}

- (void)animateMenuWithProgress:(CGFloat)progress {
    UIViewController *menuViewController = self.masterVewController;
    NSInteger location = (SLIDE_MOVEMENT * -1) + (SLIDE_MOVEMENT * progress);
    location = (location > 0) ? 0 : location;
    CGRect rect = menuViewController.view.frame;
    
    rect.origin.x = location;
    
    menuViewController.view.frame = rect;
}

- (CGFloat)horizontalLocation {
    CGRect rect = self.detailVewController.view.frame;
    return rect.origin.x;
}

- (CGFloat)horizontalSize {
    CGRect rect = self.detailVewController.view.frame;
    return rect.size.width;
}

- (NSInteger)minXForDragging {
    return (self.horizontalSize - self.slideOffset)  * -1;
}

- (NSInteger)maxXForDragging {
    return self.horizontalSize - self.slideOffset;
}

- (CGFloat)landscapeSlideOffset {
    return _landscapeSlideOffset = MENU_DEFAULT_SLIDE_OFFSET;
}

- (CGFloat)portraitSlideOffset {
    return  _portraitSlideOffset = MENU_DEFAULT_SLIDE_OFFSET;
}

- (CGFloat)slideOffset {
    return (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    ? self.landscapeSlideOffset
    : self.portraitSlideOffset;
}

- (void)enableTapGestureToCloseMenu:(BOOL)enable {
    if (enable)
    {
        [self.detailVewController.view addGestureRecognizer:self.tapRecognizer];
    }
    else
    {
        [self.detailVewController.view removeGestureRecognizer:self.tapRecognizer];
    }
}

- (void)setEnableSwipeGesture:(BOOL)markEnableSwipeGesture {
    _enableSwipeGesture = markEnableSwipeGesture;
    if (self.enableSwipeGesture)
    {
        [self.view addGestureRecognizer:self.panRecognizer];
    }
    else
    {
        [self.view removeGestureRecognizer:self.panRecognizer];
    }
}

- (void)setEnableShadow:(BOOL)enable {
    _enableShadow = enable;
    if (enable)
    {
        self.detailVewController.view.layer.shadowColor = [UIColor darkGrayColor].CGColor;
        self.detailVewController.view.layer.shadowRadius = MENU_SHADOW_RADIUS;
        self.detailVewController.view.layer.shadowOpacity = MENU_SHADOW_OPACITY;
        self.detailVewController.view.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.view.bounds].CGPath;
        self.detailVewController.view.layer.shouldRasterize = YES;
        self.detailVewController.view.layer.rasterizationScale = [UIScreen mainScreen].scale;
    }
    else
    {
        self.detailVewController.view.layer.shadowOpacity = 0;
        self.detailVewController.view.layer.shadowRadius = 0;
    }
}

- (BOOL)isMenuOpen {
    return (self.horizontalLocation == 0) ? NO : YES;
}

- (UIBarButtonItem *)getLeftBarButtonItem {
    UIBarButtonItem *leftButton =  [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuActionIcon"] style:UIBarButtonItemStylePlain target:self action:@selector(leftButtonClicked:)];
    return leftButton;
}

- (void)leftButtonClicked:(id) sender {
    if ([self isMenuOpen])
    {
        [self closeMenuWithCompletion:^{
        }];
    }
    else
    {
        [self openMenuWithCompletion:^{
        }];
    }
}

- (void)toggleHideLeftMenuButton {
    UINavigationController *currentNavController = [self getCurrentNavigationController];
    
    if (nil != currentNavController)
    {
        UIViewController *visibleVC = [self getVisibleViewController];
        UIBarButtonItem *tempItem = [visibleVC.navigationItem leftBarButtonItem];
        tempItem = (nil != tempItem) ? nil : [self getLeftBarButtonItem];
        [self setEnableSwipeGesture:(nil != tempItem)];
        [visibleVC.navigationItem setLeftBarButtonItem:tempItem animated:YES];
    }
}

#pragma mark - Gesture Recognizing -

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (self.panGestureSideOffset == 0)
    {
        return YES;
    }
    
    CGPoint pointInView = [touch locationInView:self.detailVewController.view];
    CGFloat horizontalSize = [self horizontalSize];
    
    return (pointInView.x <= self.panGestureSideOffset || pointInView.x >= horizontalSize - self.panGestureSideOffset)
    ? YES
    : NO;
}

- (void)tapDetected:(UITapGestureRecognizer *)tapRecognizer {
    [self closeMenuWithCompletion:nil];
}

- (void)panDetected:(UIPanGestureRecognizer *)aPanRecognizer {
    CGPoint translation = [aPanRecognizer translationInView:aPanRecognizer.view];
    CGPoint velocity = [aPanRecognizer velocityInView:aPanRecognizer.view];
    NSInteger movement = translation.x - self.draggingPoint.x;

    if (aPanRecognizer.state == UIGestureRecognizerStateBegan)
    {
        self.draggingPoint = translation;
    }
    else if (aPanRecognizer.state == UIGestureRecognizerStateChanged)
    {
        static CGFloat lastHorizontalLocation = 0;
        CGFloat newHorizontalLocation = [self horizontalLocation];
        lastHorizontalLocation = newHorizontalLocation;
        newHorizontalLocation += movement;

        if (newHorizontalLocation >= self.minXForDragging && newHorizontalLocation <= self.maxXForDragging && newHorizontalLocation >= 0)
        {
            [self moveHorizontallyToLocation:newHorizontalLocation];
        }
        
        self.draggingPoint = translation;
    }
    else if (aPanRecognizer.state == UIGestureRecognizerStateEnded)
    {
        NSInteger currentX = [self horizontalLocation];
        NSInteger currentXOffset = (currentX > 0) ? currentX : currentX * -1;
        NSInteger positiveVelocity = (velocity.x > 0) ? velocity.x : velocity.x * -1;
        
        if (positiveVelocity >= MENU_FAST_VELOCITY_FOR_SWIPE_FOLLOW_DIRECTION)
        {
            if (velocity.x > 0)
            {
                if (currentX > 0)
                {
                    [self openMenuWithDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
                }
                else
                {
                    [self closeMenuWithDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
                }
            }
            // Moving Left
            else
            {
                if (currentX > 0)
                {
                    [self closeMenuWithDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
                }
                else
                {
                    [self openMenuWithDuration:MENU_QUICK_SLIDE_ANIMATION_DURATION andCompletion:nil];
                }
            }
        }
        else
        {
            if (currentXOffset < (self.horizontalSize - self.slideOffset)/2)
            {
                [self closeMenuWithCompletion:nil];
            }
            else
            {
                [self openMenuWithCompletion:nil];
            }
        }
    }
}

@end
