//
//  RootViewController.m
//  SampleSplit
//
//  Created by Nilesh Jaiswal on 2/19/16.
//  Copyright © 2016 Nilesh Jaiswal. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor redColor]];
    
    self.title = @"Root View Controller";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
