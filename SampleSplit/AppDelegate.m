//
//  AppDelegate.m
//  SampleSplit
//
//  Created by Nilesh Jaiswal on 2/19/16.
//  Copyright © 2016 Nilesh Jaiswal. All rights reserved.
//

#import "AppDelegate.h"
#import "SlideViewController.h"
#import "RootViewController.h"
#import "ListViewController.h"

@interface AppDelegate ()
- (UIViewController *)setupViewControllers;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.autoresizesSubviews=YES;
    self.window.rootViewController = [self setupViewControllers];
    [self.window makeKeyAndVisible];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    return YES;
}

- (UIViewController *)setupViewControllers {
    SlideViewController *slideViewController = [SlideViewController sharedSlideViewController];
    
    RootViewController *detailViewController = [[RootViewController alloc] init];
    UINavigationController *detailNavController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
    detailNavController.navigationBar.backgroundColor = [UIColor orangeColor];
    
    ListViewController *listViewController = [[ListViewController alloc] init];
    UINavigationController *listNavController = [[UINavigationController alloc] initWithRootViewController:listViewController];
    listNavController.navigationBar.backgroundColor = [UIColor greenColor];
    
    [slideViewController setDetailVewController:detailNavController];
    [slideViewController setMasterVewController:listNavController];
    return slideViewController;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
