//
//  ListViewController.m
//  SampleSplit
//
//  Created by Nilesh Jaiswal on 2/19/16.
//  Copyright © 2016 Nilesh Jaiswal. All rights reserved.
//

#import "ListViewController.h"
#import "SlideViewController.h"
#import "RootViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"


@interface ListViewController ()
{
}

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.clearsSelectionOnViewWillAppear = NO;
    self.title = @"List VC";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"reuseIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    NSString *textStr = [NSString stringWithFormat:@"The Row Number is %ld", (long)(indexPath.row + 1)];
    
    if (indexPath.row == 0)
    {
        textStr = @"First ViewController";
    }
    else if (indexPath.row == 1)
    {
        textStr = @"Second ViewController";
    }
    else
    {
        textStr = @"Third ViewController";
    }
    
    cell.textLabel.text = textStr;

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    id detailCV = nil;
    
    if (indexPath.row == 0)
    {
        RootViewController *detailViewController = [[RootViewController alloc] init];
        UINavigationController *detailNavController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
        detailNavController.navigationBar.backgroundColor = [UIColor orangeColor];
        detailCV = detailNavController;
    }
    else if (indexPath.row == 1)
    {
        SecondViewController *secondViewController = [[SecondViewController alloc] init];
        UINavigationController *secondNavController = [[UINavigationController alloc] initWithRootViewController:secondViewController];
        secondNavController.navigationBar.backgroundColor = [UIColor greenColor];
        detailCV = secondNavController;
    }
    else
    {
        ThirdViewController *thirdViewController = [[ThirdViewController alloc] init];
        UINavigationController *thirdNavController = [[UINavigationController alloc] initWithRootViewController:thirdViewController];
        thirdNavController.navigationBar.backgroundColor = [UIColor greenColor];
        detailCV = thirdNavController;
    }
    
    [[SlideViewController sharedSlideViewController] setDetailVewController:detailCV];
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
